package com.zftlive.spider.beiyou;

import java.io.Serializable;

/**
 * 爬虫数据模型Bean
 * 
 * @author 曾繁添
 *
 */
public class RowDataBean implements Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = -3423739819942893347L;

  /**
   * 标题
   */
  private String title = "";
  
  /**
   * 内容
   */
  private String content = "";
  
  /**
   * 提取参考邮箱地址
   */
  private String email = "";
  
  /**
   * 请求URL
   */
  private String requestURL = "";
  
  /**
   * 请求Header
   */
  private String requestHeader = "";
  
  /**
   * 响应字符串
   */
  private String response = "";

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getContent() {
    return content;
  }

  public void setContent(String content) {
    this.content = content;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getRequestURL() {
    return requestURL;
  }

  public void setRequestURL(String requestURL) {
    this.requestURL = requestURL;
  }

  public String getRequestHeader() {
    return requestHeader;
  }

  public void setRequestHeader(String requestHeader) {
    this.requestHeader = requestHeader;
  }

  public String getResponse() {
    return response;
  }

  public void setResponse(String response) {
    this.response = response;
  }
}
