package com.zftlive.spider.beiyou;

/**
 * 爬虫业务处理类<br>
 * 
 * 抓取北邮论坛招聘板块招聘企业以及其对应的招聘邮箱-->http://bbs.byr.cn/#!board/JobInfo
 * 
 * @author 曾繁添
 * @version 1.0
 *
 */
public class SpiderHandler {

  /**
   * 文章列表数据请求地址，动态替换当前请求页码
   */
  private final static String ARTCLE_LIST_DATA_URL = "http://bbs.byr.cn/board/JobInfo?p=%s&_uid=guest";
  
  /**
   * 文章详情数据请求地址，动态替换artcleId即可
   */
  private final static String ARTCLE_DETAIL_DATA_URL ="http://bbs.byr.cn/article/JobInfo/%s?_uid=guest";
  
  /**
   * 分页标签样式标记，读取最大分页数
   */
  private final static String CSS_PAGENATION_BAR = "pagination";//"page-main";
  
  /**
   * 文章详情样式标记，提取文章内容
   */
  private final static String CSS_DETAIL_CONTENT = "a-content-wrap";
  
  /**
   * 网络连接超时时间，默认10s
   */
  private final static int TIME_OUT = 10 * 1000;
  
  /**
   * 总页数
   */
  private int mTotalPageNumber = 1;
  
  /**
   * 抓取结果文件输出目录
   */
  private  String resultOutputPath = "";
  
  public SpiderHandler(){
    //文件存储目录
    resultOutputPath = System.getProperty("user.dir")+"/";
  }
  
  
}
