package com.zftlive.spider.tool;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;

/**
 * 文件操作相关工具类
 * 
 * @author 曾繁添
 * @version 1.0
 */
public class ToolFile {

	private static final String TAG = ToolFile.class.getSimpleName();

	/**
	 * 以行为单位读取文件内容，一次读一整行，常用于读面向行的格式化文件
	 * @param filePath 文件路径
	 */
	public static String readFileByLines(String filePath) throws IOException
	{
		BufferedReader reader = null;
		StringBuffer sb = new StringBuffer();
		try
		{
			reader = new BufferedReader(new InputStreamReader(new FileInputStream(filePath),System.getProperty("file.encoding")));
			String tempString = null;
			while ((tempString = reader.readLine()) != null)
			{
				sb.append(tempString);
				sb.append("\n");
			}
			reader.close();
		} catch (IOException e)
		{
			e.printStackTrace();
		} finally
		{
			if (reader != null){reader.close();}
		}

		return sb.toString();

	}
	
	/**
	 * 以行为单位读取文件内容，一次读一整行，常用于读面向行的格式化文件
	 * @param filePath 文件路径
	 * @param encoding 写文件编码
	 */
	public static String readFileByLines(String filePath,String encoding) throws IOException
	{
		BufferedReader reader = null;
		StringBuffer sb = new StringBuffer();
		try
		{
			reader = new BufferedReader(new InputStreamReader(new FileInputStream(filePath),encoding));
			String tempString = null;
			while ((tempString = reader.readLine()) != null)
			{
				sb.append(tempString);
				sb.append("\n");
			}
			reader.close();
		} catch (IOException e)
		{
			e.printStackTrace();
		} finally
		{
			if (reader != null){reader.close();}
		}

		return sb.toString();
	}
	
	
	/**
	 * 保存内容
	 * @param filePath 文件路径
	 * @param content 保存的内容
	 * @throws IOException
	 */
	public static void saveToFile(String filePath,String content) throws IOException
	{
		saveToFile(filePath,content,System.getProperty("file.encoding"));
	}

	/**
	 * 指定编码保存内容
	 * @param filePath 文件路径
	 * @param content 保存的内容
	 * @param encoding 写文件编码
	 * @throws IOException
	 */
	public static void saveToFile(String filePath,String content,String encoding) throws IOException
	{
		BufferedWriter writer = null;
		File file = new File(filePath);
		try
		{
			if(!file.getParentFile().exists())
			{
				file.getParentFile().mkdirs();
			}
			writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file, false), encoding));
			writer.write(content);

		} finally
		{
			if (writer != null){writer.close();}
		}
	}
	
	/**
	 * 追加文本
	 * @param content 需要追加的内容
	 * @param file 待追加文件源
	 * @throws IOException
	 */
	public static void appendToFile(String content, File file) throws IOException
	{
		appendToFile(content, file, System.getProperty("file.encoding"));
	}

	/**
	 * 追加文本
	 * @param content 需要追加的内容
	 * @param file 待追加文件源
	 * @param encoding 文件编码
	 * @throws IOException
	 */
	public static void appendToFile(String content, File file, String encoding) throws IOException
	{
		BufferedWriter writer = null;
		try
		{
			if(!file.getParentFile().exists())
			{
				file.getParentFile().mkdirs();
			}
			writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file, true), encoding));
			writer.write(content);
		} finally
		{
			if (writer != null){writer.close();}
		}
	}
	
	/**
	 * 判断文件是否存在
	 * @param filePath 文件路径
	 * @return 是否存在
	 * @throws Exception
	 */
	public static Boolean isExsit(String filePath)
	{
		Boolean flag = false ;
		try
		{
			File file = new File(filePath);
			if(file.exists())
			{
				flag = true;
			}
		}catch(Exception e){
			System.out.println("判断文件失败-->"+e.getMessage()); 
		} 
		
		return flag;
	}
	
	/**
	 * 读取指定目录文件的文件内容
	 * 
	 * @param fileName
	 *            文件名称
	 * @return 文件内容
	 * @throws Exception
	 */
	public static String read(String fileName) throws IOException {
		FileInputStream inStream = new FileInputStream(fileName);
		ByteArrayOutputStream outStream = new ByteArrayOutputStream();
		byte[] buffer = new byte[1024];
		int len = 0;
		while ((len = inStream.read(buffer)) != -1) {
			outStream.write(buffer, 0, len);
		}
		byte[] data = outStream.toByteArray();
		return new String(data);
	}

	/***
	 * 以行为单位读取文件内容，一次读一整行，常用于读面向行的格式化文件
	 * 
	 * @param fileName
	 *            文件名称
	 * @param encoding
	 *            文件编码
	 * @return 字符串内容
	 * @throws IOException
	 */
	public static String read(String fileName, String encoding)
			throws IOException {
		BufferedReader reader = null;
		StringBuffer sb = new StringBuffer();
		try {
			reader = new BufferedReader(new InputStreamReader(
					new FileInputStream(fileName), encoding));
			String tempString = null;
			while ((tempString = reader.readLine()) != null) {
				sb.append(tempString);
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (reader != null) {
				reader.close();
			}
		}

		return sb.toString();
	}

	/**
	 * 指定编码将内容写入目标文件
	 * 
	 * @param target
	 *            目标文件
	 * @param content
	 *            文件内容
	 * @param encoding
	 *            写入文件编码
	 * @throws Exception
	 */
	public static void write(File target, String content, String encoding)
			throws IOException {
		BufferedWriter writer = null;
		try {
			if (!target.getParentFile().exists()) {
				target.getParentFile().mkdirs();
			}
			writer = new BufferedWriter(new OutputStreamWriter(
					new FileOutputStream(target, false), encoding));
			writer.write(content);

		} finally {
			if (writer != null) {
				writer.close();
			}
		}
	}
	
	/**
	 * 指定目录写入文件内容
	 * @param filePath 文件路径+文件名
	 * @param content 文件内容
	 * @throws IOException
	 */
	public static void write(String filePath, byte[] content)
			throws IOException {
		FileOutputStream fos = null;

		try {
			File file = new File(filePath);
			if (!file.getParentFile().exists()) {
				file.getParentFile().mkdirs();
			}
			fos = new FileOutputStream(file);
			fos.write(content);
			fos.flush();
		} finally {
			if (fos != null) {
				fos.close();
			}
		}
	}
	
	/**
	 * 写入文件
	 * 
	 * @param inputStream下载文件的字节流对象
	 * @param filePath文件的存放路径(带文件名称)
	 * @throws IOException 
	 */
	public static File write(InputStream inputStream, String filePath) throws IOException {
		OutputStream outputStream = null;
		// 在指定目录创建一个空文件并获取文件对象
		File mFile = new File(filePath);
		if (!mFile.getParentFile().exists())
			mFile.getParentFile().mkdirs();
		try {
			outputStream = new FileOutputStream(mFile);
			byte buffer[] = new byte[4 * 1024];
			int lenght = 0 ;
			while ((lenght = inputStream.read(buffer)) > 0) {
				outputStream.write(buffer,0,lenght);
			}
			outputStream.flush();
			return mFile;
		} catch (IOException e) {
//			Log.e(TAG, "写入文件失败，原因："+e.getMessage());
			throw e;
		}finally{
			try {
				inputStream.close();
				outputStream.close();
			} catch (IOException e) {
			}
		}
	}
	
	/**
	 * 序列化对象
	 * @param rsls 需要序列化的对象
	 * @param filename 文件名
	 */
	public static synchronized <T> void serializeObject(T rsls, String filename) {
		try {
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			ObjectOutputStream oos = new ObjectOutputStream(baos);
			oos.writeObject(rsls);
			byte[] data = baos.toByteArray();
			OutputStream os = new FileOutputStream(new File(filename));
			os.write(data);
			os.flush();
			os.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 反序列化对象
	 * @param filename 文件名
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static synchronized <T> T deserializeObject(String filename) {
		T obj = null;
		try {
			FileInputStream fis = new FileInputStream(filename);
			ObjectInputStream ois = new ObjectInputStream(fis);
			while (fis.available() > 0) {
				obj = (T) ois.readObject();
			}
			ois.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return obj;
	}
	
	/**
	 * 删除指定路径的文件
	 * @param filePath 文件路径
	 */
	public static void deleteFile(String filePath){
	  if(null == filePath || "".equals(filePath))return;
	  File file = new File(filePath);
	  if(file.exists()){
	    file.delete();
	  }
	}
}
